<?php
require_once 'helpers.php';
checkLogin();
$connection = mysqli_connect("localhost", "root", "password", "notes");
$query = "SELECT * FROM notes ORDER BY position;";
$result = mysqli_query($connection, $query);
$notes = mysqli_fetch_all($result, MYSQLI_ASSOC);
$close = mysqli_close($connection);

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Notes</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/water.css@2/out/dark.min.css">
</head>
<body>
    <h1>Home</h1>
    <a href="../notes/users/show.php">Account</a>
    |
    <a href="session/logout.php">Logout</a>
    <hr>
    <ul>
        <li><a href="notes/index.php">Notes</a></li>
        <li><a href="categories/index.php">Categories</a></li>
    </ul>
    </body>
</html>