<?php

require_once "../vendor/autoload.php";

session_start();

if (isset($_SESSION['user'])) {
    header('Location: /notes/index.php');
    exit(0);
}

if (!isset($_POST['email']) || !isset($_POST['password'])) {
    header('Location: login.php');
    exit(0);
}

$connection = mysqli_connect("localhost", "root", "password", "notes");
$email = $_POST['email'];
$password = $_POST['password'];
$query = "SELECT * FROM users WHERE email = '" . $email . "' AND password = '" . $password . "';";
$result = mysqli_query($connection, $query);
$user = mysqli_fetch_assoc($result);

if ($user == null) {
    header('Location: login.php');
    exit(0);
}

$_SESSION['user'] = $user;
header('Location: /notes/index.php');
exit(0);

