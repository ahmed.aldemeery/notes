<?php

if (!function_exists('dump')) {
    function dump(...$args)
    {
        echo "<pre>";
        foreach ($args as $arg) {
            print_r($arg);
            echo "<br>";
        }
        echo "</pre>";
    }
}

if (!function_exists('dd')) {
    function dd(...$args)
    {
        dump(...$args);
        die();
    }
}

if (!function_exists('flatten')) {
    function flatten($array)
    {
        $return = array();
        array_walk_recursive($array, function($a) use (&$return) { $return[] = $a; });
        return $return;
    }
}

if (!function_exists('checkLogin')) {
    function checkLogin()
    {
        @session_start();
        if (!isset($_SESSION['user'])) {
            header('Location: /notes/session/login.php');
            exit(0);
        }
    }
}

if (!function_exists('user')) {
    function user()
    {
        @session_start();

        $user = isset($_SESSION['user']) ? $_SESSION['user'] : null;

        return $user;
    }
}

if (!function_exists('userId')) {
    function userId()
    {
        @session_start();
        
        $userId = isset($_SESSION['user']['id']) ? $_SESSION['user']['id'] : null;
        
        return $userId;
    }
}

if (!function_exists('authorize')) {
    function authorize($userId)
    {
        if ($userId != userId()) {
            header('Location: /notes/404.php');
            exit(0);
        }        
    }
}