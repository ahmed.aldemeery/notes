<?php

require_once "../helpers.php";

checkLogin();

$note_id = $_GET['id'];

$connection = mysqli_connect("localhost", "root", "password","notes");

$result = mysqli_query($connection, "SELECT * FROM notes WHERE id = " . $note_id . ";");
$note = mysqli_fetch_assoc($result);

authorize($note['user_id']);

$query = "UPDATE `notes` SET 
    `title` = '" . $_POST['title'] . "',
    `content` = '" . $_POST['content'] . "',
    `position` = " . $_POST['position'] . " 
    WHERE `id` = " . $note_id . ";";

mysqli_query($connection, $query);

$delete = "DELETE FROM `category_note` WHERE `note_id` = " . $note_id . ";";
mysqli_query($connection, $delete);
    
// 3. Insert into pivot table
$insert = "INSERT INTO `category_note`(`category_id`, `note_id`) VALUES ";

foreach ($_POST['categories'] as $category_id) {
    $insert = $insert . "($category_id, $note_id),";
}

$insert = rtrim($insert, ",");
$insert = $insert . ";";

mysqli_query($connection, $insert);
header("Location: index.php");