
<?php
require_once "../helpers.php";

checkLogin();

$connection = mysqli_connect("localhost", "root", "password", "notes");
$query = "SELECT * FROM categories WHERE user_id = " . userId() . ";";
$result = mysqli_query($connection, $query);
$categories = mysqli_fetch_all($result, MYSQLI_ASSOC);
$close = mysqli_close($connection);
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Create</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/water.css@2/out/dark.min.css">
</head>
<body>
    <h1>Create Note</h1>
    <a href="/notes/index.php">Home</a>
    |
    <a href="index.php">All Notes</a>
    <hr>
    <form action="store.php" method="post">
        <label for="title">Title</label>
        <input type="text" name="title" id="title" required>
        <label for="category">Category</label>
        <select name="categories[]" id="categories" multiple>
        <?php foreach ($categories as $category) {?> 
            <option  value="<?php echo $category['id'];?>" ><?php echo $category['name'];?></option>
        <?php }?>
        </select>
        <label for="position">Position</label>
        <input type="number" name="position" id="position" required>
        <label for="content">Content</label>
        <textarea name="content" id="content" cols="80" rows="15" required></textarea>
        <input type="reset" value="Reset" style="display: inline;">
        <input type="submit" value="Create" style="display: inline;">
    </form>
</body>
</html>