<?php
require_once "../helpers.php";

checkLogin();

$connection = mysqli_connect("localhost", "root", "password","notes");

// 1. Create note
$query = "INSERT INTO `notes`(`user_id`, `title`, `position`,`content`) VALUES (
    " . userId() .",
    '" . $_POST["title"] . "',
    " . $_POST["position"] . ",
    '" . $_POST["content"] . "'
);";
mysqli_query($connection, $query);

// 2. Get created note id
$note_id = mysqli_insert_id($connection);

// 3. Insert into pivot table
$insert = "INSERT INTO `category_note`(`category_id`, `note_id`) VALUES ";

foreach ($_POST['categories'] as $category_id) {
    $insert = $insert . "($category_id, $note_id),";
}

$insert = rtrim($insert, ",");
$insert = $insert . ";";

mysqli_query($connection, $insert);
header("Location: index.php");