<?php
require_once "../helpers.php";

checkLogin();

$connection = mysqli_connect("localhost", "root", "password", "notes");
$query = "SELECT * FROM notes WHERE user_id = " . userId() . " ORDER BY position;";
$result = mysqli_query($connection, $query);
$notes = mysqli_fetch_all($result, MYSQLI_ASSOC);
$close = mysqli_close($connection);

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Notes</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/water.css@2/out/dark.min.css">
</head>
    <body>
    <h1>All Notes</h1>
    <a href="/notes/index.php">Home</a>
    |
    <a href="create.php">New Note</a>
    <hr>
    <table> 
        <tr>
            <th>Title</th>
            <th>Content</th>
            <th>Created At</td>
            <th>Actions</td>
        </tr>
        <?php foreach ($notes as $note) {?> 
            <tr>
                <td><?= $note['title'] ?></td>
                <td><?= substr($note['content'], 0, 20); ?>...</td>
                <td><?= $note['created_at'] ?></td>
                <td>
                    <a href="show.php?id=<?php echo $note["id"];?>">View</a>
                    |
                    <a href="edit.php?id=<?php echo $note["id"];?>">Edit</a>
                    |
                    <a href="delete.php?id=<?php echo $note["id"];?>">Delete</a>
                </td>
            </tr>
        <?php }?>
    </table> 
    </body>
</html>