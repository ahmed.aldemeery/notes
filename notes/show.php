<?php
require_once "../vendor/autoload.php";
require_once "../helpers.php";

checkLogin();
$connection = mysqli_connect("localhost", "root", "password", "notes");
$id = $_GET["id"];
$query = "SELECT * FROM notes WHERE id = ". $id;
$result = mysqli_query($connection, $query);
$note = mysqli_fetch_assoc($result);

authorize($note['user_id']);

$close = mysqli_close($connection);
?>



<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=, initial-scale=1.0">
    <title>View</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/water.css@2/out/dark.min.css">
</head>
<body>
    <h1><?php echo $note["title"];?></h1>
    <span><?php echo $note["created_at"];?></span>
    <br>
    <a href="edit.php?id=<?php echo $note["id"];?>">Edit</a>
    |
    <a href="delete.php?id=<?php echo $note["id"];?>">Delete</a>
    <hr>
    <p>
        <?php echo $note["content"];?>
    </p>

</body>
</html>