<?php
require_once "../vendor/autoload.php";
require_once "../helpers.php";

checkLogin();

$connection = mysqli_connect("localhost", "root", "password", "notes");
$id = $_GET["id"];

$categoriesQuery = "SELECT * FROM categories WHERE user_id=" . userId() . ";";
$noteQuery = "SELECT * FROM notes WHERE id = ". $id;
$selectedCategoriesQuery = "SELECT category_id FROM category_note WHERE note_id = " . $id . ";";

$categoriesResult = mysqli_query($connection, $categoriesQuery);
$noteResult = mysqli_query($connection, $noteQuery);
$selectedResult = mysqli_query($connection, $selectedCategoriesQuery);

$categories = mysqli_fetch_all($categoriesResult, MYSQLI_ASSOC);
$note = mysqli_fetch_assoc($noteResult);
$selected = array_flatten(mysqli_fetch_all($selectedResult, MYSQLI_ASSOC));

authorize($note['user_id']);

mysqli_close($connection); 
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Update</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/water.css@2/out/dark.min.css">
</head>
<body>
    <h1>Edit Note</h1>
    <a href="/notes/index.php">Home</a>
    |
    <a href="index.php">All Notes</a>
    <hr>
    <form action="update.php?id=<?php echo $note['id'];?>" method="post">
        <label for="title">Title</label>
        <input type="text" name="title" id="title" value="<?php echo $note["title"];?>" required>
        <label for="category">Category</label>
        <select name="categories[]" id="categories" multiple>
        <?php foreach ($categories as $category) {?> 
            <?php if (in_array($category['id'], $selected)) { ?>
                <option  value="<?php echo $category['id'];?>" selected><?php echo $category['name'];?></option>
            <?php } else { ?>
                <option  value="<?php echo $category['id'];?>"><?php echo $category['name'];?></option>
            <?php } ?>
        <?php }?>
        <label for="content">Position</label>
        <input type="number" name="position" id="position"  value="<?php echo $note["position"];?>" required>
        <label for="content">Content</label>
        <textarea name="content" id="content" cols="80" rows="15" required><?php echo $note["content"];?></textarea>
        <input type="reset" value="Reset" style="display: inline;">
        <input type="submit" value="Edit" style="display: inline;">
    </form>
</body>
</html>