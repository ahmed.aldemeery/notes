    CREATE DATABASE `notes`;

    USE `notes`;

    CREATE TABLE `users` (
        `id` INT(11) NOT NULL AUTO_INCREMENT,
        `name` VARCHAR(255) NOT NULL,
        `email` VARCHAR(255) NOT NULL UNIQUE,
        `password` VARCHAR(255) NOT NULL,
        `created_at` TIMESTAMP NOT NULL DEFAULT NOW(),
        PRIMARY KEY(`id`)
    );

    CREATE TABLE `categories` (
        `id` INT(11) NOT NULL AUTO_INCREMENT,
        `user_id` INT(11) NOT NULL,
        `name` VARCHAR(255) NOT NULL,
        `position`INT (11),
        `created_at` TIMESTAMP NOT NULL DEFAULT NOW(),
        UNIQUE (`name`, `user_id`),
        PRIMARY KEY(`id`),
        FOREIGN KEY(`user_id`) REFERENCES `users`(`id`)
            ON UPDATE CASCADE
            ON DELETE CASCADE
    );

    CREATE TABLE `notes` (
        `id` INT(11) NOT NULL AUTO_INCREMENT,
        `user_id` INT(11) NOT NULL, 
        `title` VARCHAR(255) NOT NULL,
        `content` VARCHAR(255) NOT NULL,
        `position` TINYINT(3) NOT NULL,
        `created_at` TIMESTAMP NOT NULL DEFAULT NOW(),
        PRIMARY KEY(`id`),
        FOREIGN KEY(`user_id`) REFERENCES `users`(`id`)
            ON UPDATE CASCADE
            ON DELETE CASCADE
    );

    CREATE TABLE `category_note` (
        `id` INT(11) NOT NULL AUTO_INCREMENT,
        `category_id` INT(11) NOT NULL,
        `note_id` INT(11) NOT NULL,
        UNIQUE (`category_id`, `note_id`),
        PRIMARY KEY(`id`),
        FOREIGN KEY(`category_id`) REFERENCES `categories`(`id`)
            ON UPDATE CASCADE
            ON DELETE CASCADE,
        FOREIGN KEY(`note_id`) REFERENCES `notes`(`id`)
            ON UPDATE CASCADE
            ON DELETE CASCADE
    );


