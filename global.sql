-- ===============================
-- Database
-- ===============================
CREATE DATABASE `global`;
USE `global`;

-- ===============================
-- Users table
-- ===============================
CREATE TABLE `users` ( 
    `id` INT(11) NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(255) NOT NULL,
    `email` VARCHAR(255) NOT NULL,
    `password` VARCHAR(255) NOT NULL,
    `about` TEXT,
    `created_at` TIMESTAMP NOT NULL DEFAULT NOW(),
    PRIMARY KEY(`id`),
    UNIQUE KEY(`email`)
);

-- ===============================
-- Ads table
-- ===============================
CREATE TABLE `ads` (
    `id` INT(11) NOT NULL AUTO_INCREMENT,
    `user_id` INT(11) NOT NULL,
    `title` VARCHAR(255) NOT NULL,
    `content` TEXT NOT NULL,
    `visible` TINYINT(1) NOT NULL DEFAULT 0,
    `created_at` TIMESTAMP NOT NULL DEFAULT NOW(),
    PRIMARY KEY(`id`),
    FOREIGN KEY(`user_id`) REFERENCES `users`(`id`)
);

-- ===============================
-- Pages table
-- ===============================
CREATE TABLE `pages` (
    `id` INT(11) NOT NULL AUTO_INCREMENT,
    `user_id` INT(11) NOT NULL,
    `title` VARCHAR(255) NOT NULL,
    `position` TINYINT(3) NOT NULL,
    `visible` TINYINT(1) NOT NULL DEFAULT 0,
    `created_at` TIMESTAMP NOT NULL DEFAULT NOW(),
    PRIMARY KEY(`id`),
    FOREIGN KEY(`user_id`) REFERENCES `users`(`id`)
);

-- ===============================
-- Subjects table
-- ===============================
CREATE TABLE `subjects` (
    `id` INT(11) NOT NULL AUTO_INCREMENT,
    `user_id` INT(11) NOT NULL,
    `title` VARCHAR(255) NOT NULL,
    `position` TINYINT(3) NOT NULL, 
    `visible` TINYINT(1) NOT NULL DEFAULT 0,
    `created_at` TIMESTAMP NOT NULL DEFAULT NOW(),
    PRIMARY KEY(`id`),
    FOREIGN KEY(`user_id`) REFERENCES `users`(`id`)
);

-- ===============================
-- INSERT
-- ===============================

INSERT INTO `users`(`name`, `email`, `password`, `about`) VALUES (
    'ahmed',
    'ahmed@mail.com',
    'password1',
    'ahmed elbaz mohamed'
), (
    'osama',
    'osama@mail.com',
    'password2',
    'osama elbaz mohamed'
);

INSERT INTO `ads`(`user_id`, `title`, `content`, `visible`) VALUES (
    1,
    'php course',
    'php php',
    1
), (
    1,
    'mysql course',
    'mysql mysql',
    1
), (
    2,
    'programming course',
    'programming programming',
    1
);

INSERT INTO `pages`(`user_id`, `title`, `position`, `visible`) VALUES (
    1,
    'page1',
    1,
    1
);

INSERT INTO `subjects`(`user_id`, `title`, `position`, `visible`) VALUES (
    1,
    'subject1',
    1,
    1
);


-- ================================
-- SELECT
-- ================================
SELECT * FROM `ads`;

SELECT  `id`, `email` as `username`, `password` FROM `users`;

SELECT * FROM `ads` WHERE `user_id` = 1 AND title = 'php course' OR id = 3;

-- ================================
-- UPDATE
-- ================================
UPDATE `ads` SET `user_id` = 2, `title` = 'java course' WHERE `id` = 1;

-- ================================
-- DELETE 
-- ================================
DELETE FROM `ads` WHERE `id` = 3;

-- ================================
-- JOINS 
-- ================================
SELECT * FROM `ads`
JOIN `users` ON `users`.`id` = `ads`.`user_id`
WHERE `users`.`name` = 'ahmed';

