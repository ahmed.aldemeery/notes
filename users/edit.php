<?php
require_once "../helpers.php";

checkLogin();

$connection = mysqli_connect("localhost", "root", "password", "notes");
$id = userId();
$query = "SELECT * FROM users WHERE id = " . $id . ";";
$result = mysqli_query($connection, $query);
$user = mysqli_fetch_assoc($result);
mysqli_close($connection); 

?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Update</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/water.css@2/out/dark.min.css">
</head>
<body>
    <h1>Edit Account</h1>
    <a href="../index.php">Home</a>
    <hr>
    <form action="update.php?id=<?php echo $user['id']; ?>" method="post">
        <label for="name">Name</label>
        <input type="text" name="name" id="name" value="<?php echo $user["name"];?>" required>
        <label for="name">Email</label>
        <input type="email" name="email" id="email" value="<?php echo $user["email"];?>" required>
        <label for="name">Password</label>
        <input type="password" name="password" id="password" value="<?php echo $user["password"];?>" required>
        <input type="reset" value="Reset" style="display: inline;">
        <input type="submit" value="Edit" style="display: inline;">
    </form>
</body>
</html>
