
<?php
require_once "../helpers.php";

$connection = mysqli_connect("localhost", "root", "password", "notes");
$query = "SELECT * FROM categories";
$result = mysqli_query($connection, $query);
$categories = mysqli_fetch_all($result, MYSQLI_ASSOC);
$close = mysqli_close($connection);
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Create</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/water.css@2/out/dark.min.css">
</head>
<body>
    <h1>New User</h1>
    <a href="../session/login.php">Login</a>
    <hr>
    <form action="store.php" method="post">
        <label for="name">Name</label>
        <input type="text" name="name" id="name" required>
        <label for="email">Email</label>
        <input type="text" name="email" id="email" required>
        <label for="password">Password</label>
        <input type="password" name="password" id="password" required>
        <label for="password">Password Confirmation</label>
        <input type="password" name="password_confirm" id="password" required>
        <input type="reset" value="Reset" style="display: inline;">
        <input type="submit" value="Create" style="display: inline;">
    </form>
</body>
</html>