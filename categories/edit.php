<?php
require_once "../helpers.php";

checkLogin();

$connection = mysqli_connect("localhost", "root", "password", "notes");
$id = $_GET["id"];
$query = "SELECT * FROM categories WHERE id = " . $id . ";";
$result = mysqli_query($connection, $query);
$category = mysqli_fetch_assoc($result);
authorize($category['user_id']);

?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Update</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/water.css@2/out/dark.min.css">
</head>
<body>
    <h1>Edit Categories</h1>
    <hr>
    <form action="update.php?id=<?php echo $category['id']; ?>" method="post">
        <label for="name">Name</label>
        <input type="text" name="name" id="name" value="<?php echo $category["name"];?>" required>
        <label for="position">Position</label>
        <input type="number" name="position" id="position"  value="<?php echo $category["position"];?>" required>
        <input type="reset" value="Reset" style="display: inline;">
        <input type="submit" value="Edit" style="display: inline;">
    </form>
</body>
</html>
