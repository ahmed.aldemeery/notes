<?php
require_once "../helpers.php";

checkLogin();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Create</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/water.css@2/out/dark.min.css">
</head>
    <body>
        <h1>Create Category</h1>
        <a href="/notes/index.php">Home</a>
        |
        <a href="index.php">Back</a>
        <hr>
        <form action="store.php" method="post">
        <label for="name">Name</label>
        <input type="text" name="name" id="name" required>
        <label for="position">Position</label>
        <input type="number" name="position" id="position" required>
        <input type="reset" value="Reset" style="display: inline;">
        <input type="submit" value="Create" style="display: inline;">
    </form>
    </body>
</html>