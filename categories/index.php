<?php 

require_once "../helpers.php";

checkLogin();

$connection = mysqli_connect("localhost", "root", "password", "notes");
$query = "SELECT * FROM categories where user_id  =" .  userId() . " ORDER BY position;";
$result = mysqli_query($connection, $query);
$categories = mysqli_fetch_all($result, MYSQLI_ASSOC);
// authorize($categories['user_id']);
$close = mysqli_close($connection); 
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>categories</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/water.css@2/out/dark.min.css">
</head>
    <body>
        <h1>All categories</h1>
        <a href="/notes/index.php">Home</a>
        |
        <a href="create.php">New Category</a>
        <hr>
        <table>
            <tr>
                <th>Name</th>
                <th>Created At</th>
                <th>Actions</th>
            </tr>
            <?php foreach ($categories as $category) {?> 
            <tr>
                <td><?= $category['name'] ?></td>
                <td><?= $category['created_at'] ?></td>
                <td>
                    <a href="edit.php?id=<?php echo $category["id"];?>">Edit</a>
                    |
                    <a href="delete.php?id=<?php echo $category["id"];?>">Delete</a>
                </td>
            </tr>
        <?php }?>
        </table>
    </body>
</html>